package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private final HashMap<String, Double> bookPrices = new HashMap<>();

    public Quoter() {
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }

    double getBookPrice(String isbn) {
        return bookPrices.getOrDefault(isbn, 0.0);
    }
}
